@extends('layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">TAMBAH SUB KRITERIA <span class="badge badge-secondary" data-toggle="tooltip" data-placement="top" title="Ubah">{{$data->nama}}</span></h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item"><a href="/kriteria">Kriteria</a></li>
                <li class="breadcrumb-item active">{{$data->nama}}</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="container-fluid">
    <div class="card">
        @include ('includes.flash')
        <div class="card-body">
            <form role="form" method="post" action="{{ route('sub_kriteria.store') }}">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Kriteria</label>
                        <input type="text" class="form-control" value="{{$data->nama}}" disabled id="exampleInputEmail1" placeholder="id">
                        <input type="hidden" class="form-control" value="{{$data->id}}" name="id_kriteria" id="exampleInputEmail1" placeholder="id">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Nama Sub Kriteria</label>
                        <input type="text" class="form-control" name="nama" id="exampleInputPassword1" placeholder="nama">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Parameter</label>
                        <input type="text" class="form-control" name="parameter" id="exampleInputPassword1" placeholder="masukkan keterangan">
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</section>
@include ('includes.script')
@endsection
