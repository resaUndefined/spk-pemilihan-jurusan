@extends('layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">EDIT SUB KRITERIA</h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item"><a href="/kriteria">Kriteria</a></li>
                <li class="breadcrumb-item"><a href="/sub_kriteria/{{$data->id_kriteria}}">{{$data->kriteria->nama}}</a></li>
                <li class="breadcrumb-item active">{{$data->nama}}</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="container-fluid">
    <div class="card">
        @include ('includes.flash')
        <div class="card-body">
            <form role="form" method="post" action="{{ route('sub_kriteria.update', $data->id) }}">
                @csrf
                @method('PATCH')
                <div class="card-body">
                    <div class="form-group">
                        <label>Nama Sub Kriteria</label>
                        <input type="text" class="form-control" name="nama" value="{{$data->nama}}" placeholder="nama">
                    </div>
                    <div class="form-group">
                        <label>Parameter</label>
                        <input type="text" class="form-control" name="parameter" value="{{$data->parameter}}" placeholder="0 - 30 / 31 - 60 / 61 - 100">
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Ubah</button>
                </div>
            </form>
        </div>
    </div>
</section>
@include ('includes.script')
@endsection
