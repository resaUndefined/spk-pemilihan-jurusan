@extends('layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">Tambah User</h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active">User</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="container-fluid">
    <div class="card">
        @include ('includes.flash')
        <div class="card-body">
            <form role="form" method="post" action="{{ route('users.update',$user->id) }}">
                @csrf
                @method('put')
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Nama Pengguna</label>
                        <input type="text" class="form-control" name="name" id="exampleInputPassword1" value="{{ $user->name }}" required>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Email Pengguna</label>
                        <input type="email" class="form-control" name="email" id="exampleInputPassword1" value="{{ $user->email }}" required>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Role</label>
                        <select name="role" id="role" required class="form-control">
                            <option value="">--Pilih Role--</option>
                            <option value="admin" @if ($user->role == 'admin')
                                selected=""
                            @endif>Admin</option>
                            <option value="user" @if ($user->role == 'user')
                                selected=""
                            @endif>Pengguna</option>
                        </select>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" name="password" id="exampleInputPassword1">
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</section>
@include ('includes.script')
@endsection
