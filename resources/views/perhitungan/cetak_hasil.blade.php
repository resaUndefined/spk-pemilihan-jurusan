<html>
    <head>

    </head>
    <body>
        <div class="card">
            <div class="card-body">
                <table id="table3" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>PERINGKAT</th>
                            <th>ALTERNATIF</th>
                            @foreach($matrixHasil['header'] as $key => $value)
                            <th>{{ $value }}</th>
                            @endforeach
                            <th>TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($nilaiAlternatif['data'] as $key => $value)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $value['nama'] }}</td>
                            @foreach($value['nilai'] as $key2 => $value2)
                            <td>{{ round($value2['hasil'], 2) }}</td>
                            @endforeach
                            <td>{{ round($value['total'], 2) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
