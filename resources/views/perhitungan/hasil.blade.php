@extends('layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">HASIL PERHITUNGAN</h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active">Hasil Perhitungan</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<section class="container-fluid">
    @if(isset($matrixHasil['data']) && isset($matrixHasil['prioritas']))
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="m-0 text-dark"><strong>MATRIKS HASIL</strong></h3>
                        </div>
                        <div class="card-body">
                            <table id="example1" class="table table-bordered">
                                <tbody>
                                    <tr>
                                        @foreach ($matrixHasil['header'] as $value)
                                        <td class="bg-primary">{{ $value }}</td>
                                        @endforeach
                                    </tr>
                                    <tr>
                                        @foreach ($matrixHasil['prioritas'] as $prioritas)
                                        <td>{{round($prioritas, 2)}}</td>
                                        @endforeach
                                    </tr>
                                    @for($x=0; $x < count($matrixHasil['data'][0]['header']); $x++)
                                    <tr class="bg-secondary">
                                        @foreach ($matrixHasil['data'] as $key2 => $value2)
                                        <td>{{ $value2['header'][$x] }}</td>
                                        @endforeach
                                    </tr>
                                    <tr>
                                        @foreach ($matrixHasil['data'] as $key2 => $value2)
                                        <td>{{ round($value2['prioritas'][$x], 2) }}</td>
                                        @endforeach
                                    </tr>
                                @endfor
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3 class="m-0 text-dark"><strong>NILAI ALTERNATIF</strong></h3>
                        </div>
                        <div class="card-body">
                            <table id="example1" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>ALTERNATIF</th>
                                        @foreach($matrixHasil['header'] as $key => $value)
                                        <th>{{ $value }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($nilaiAlternatif['data'] as $key => $value)
                                    <tr>
                                        <td>{{ $value['nama'] }}</td>
                                        @foreach($value['nilai'] as $key2 => $value2)
                                        <td>{{ $value2['nama'] }}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3 class="m-0 text-dark d-inline"><strong>HASIL AKHIR</strong></h3>
                        <a href="{{ route('hasil.cetak') }}" class="btn btn-danger pull-right text-white">Export</a>
                        </div>
                        <div class="card-body">
                            <table id="table3" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="20">PERINGKAT</th>
                                        <th>ALTERNATIF</th>
                                        @foreach($matrixHasil['header'] as $key => $value)
                                        <th>{{ $value }}</th>
                                        @endforeach
                                        <th width="20">TOTAL</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($nilaiAlternatif['data'] as $key => $value)
                                    <tr>
                                        <td class="text-center">{{ $loop->iteration }}</td>
                                        <td>{{ $value['nama'] }}</td>
                                        @foreach($value['nilai'] as $key2 => $value2)
                                        <td>{{ round($value2['hasil'], 2) }}</td>
                                        @endforeach
                                        <td>{{ round($value['total'], 2) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @else
    <div class="alert alert-danger" role="alert">
        Silahkan lengkapi data bobot kriteria terlebih dahulu !
    </div>
    @endif
</section>
@include ('includes.script')
<script>
  $(function () {
    $("#table3").DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": false,
      "language": {
          "sEmptyTable":   "Tidak ada data yang tersedia pada tabel ini",
    "sProcessing":   "Sedang memproses...",
    "sLengthMenu":   "Tampilkan _MENU_ entri",
    "sZeroRecords":  "Tidak ditemukan data yang sesuai",
    "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
    "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
    "sInfoPostFix":  "",
    "sSearch":       "Cari:",
    "sUrl":          "",
    "oPaginate": {
        "sFirst":    "Pertama",
        "sPrevious": "Sebelumnya",
        "sNext":     "Selanjutnya",
        "sLast":     "Terakhir"
    }
        }
    });
  });
</script>
@endsection
