@extends('layouts.app')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h2 class="m-0 text-dark">DATA ALTERNATIF</h2>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active">Data Alternatif</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<section class="container-fluid">
    <div class="card">
        @include ('includes.flash')
        <div class="card-body">
            <h3>Tabel Data Alternatif</h3>
            <table id="data-admin" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="40">NO</th>
                        <th>ALTERNATIF</th>
                        @foreach ($kriteria as $item)
                            <th>{{ $item->nama }}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach($alternatif as $alt)
                    <tr>
                        <td class="text-center">{{$loop->iteration}}</td>
                        <td>{{ $alt->nama }}</td>
                        @foreach ($alt->data as $data)
                            <td>{{ $data }}</td>
                        @endforeach                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <hr>
        <div class="card-body">
            <h3 style="text-align: center;">Detail Data Kriteria</h3>
            <br>
            @foreach ($kriteria as $item)
                <h5>Tabel Keterangan Kriteria <b>{{ $item->nama }}</b></h5>
                <table id="data-admin" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th width="40">NO</th>
                            <th>SUB KRITERIA</th>                            
                            <th>KETERANGAN</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($item->sub_kriteria as $sub)
                        <tr>
                            <td class="text-center">{{$loop->iteration}}</td>
                            <td>{{ $sub->nama }}</td>
                            <td>{{ $sub->parameter }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <br>
                <br>
            @endforeach
        </div>
    </div>
</section>
@include ('includes.script')
@endsection
