<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Kriteria;
use App\Models\SubKriteria;

class SubKriteriaController extends Controller
{
    public function show($id)
    {
        $data = Kriteria::with('sub_kriteria')->where('id', $id)->first();
        return view('sub_kriteria.sub_kriteria')->with('data', $data);
    }

    public function create(Request $request)
    {
        $data = Kriteria::where('id', $request->id_kriteria)->first();
        if($data){
            return view('sub_kriteria.tambah_sub_kriteria')->with('data', $data);
        }
        return redirect()->route('kriteria.index');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'id_kriteria' => 'required|exists:kriteria,id',
            'nama' => 'required',
            'parameter' => 'required'
         ]);

        $data = new SubKriteria;
        $data->id_kriteria = request('id_kriteria');
        $data->nama = request('nama');
        $data->parameter = request('parameter');
        $data->save();
        return redirect('/sub_kriteria/'.request('id_kriteria'))->with('success',
        'Berhasil menambah data');
    }

    public function edit($id)
    {
        $data = SubKriteria::where('id', $id)->first();
        if($data){
            return view('sub_kriteria.edit_sub_kriteria')->with('data', $data);
        }
        return redirect()->route('sub_kriteria.index');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nama' => 'required',
            'parameter' => 'required',
         ]);

        $data = SubKriteria::where('id', $id)->first();
        if($data){
            $data->nama = request('nama');
            $data->parameter = request('parameter');
            $data->save();
        }
        return redirect('/sub_kriteria/'.$data->id_kriteria)->with('success',
        'Berhasil mengubah data');
    }

    public function destroy($id)
    {
        $data = SubKriteria::where('id', $id)->first();
        if($data){
            $data->delete();
        }
        return redirect('/sub_kriteria/'.$data->id_kriteria)->with('danger',
        'Berhasil menghapus data');
    }
}
