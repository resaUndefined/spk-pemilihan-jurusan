<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class BobotKriteria extends Model
{
    protected $fillable = [
        'id_user', 'id_kriteria_1', 'nilai', 'id_kriteria_2'
    ];
    protected $table = "bobot_kriteria";
    public $timestamps = false;
    public function sub_kriteria()
    {
        return $this->hasMany('App\Models\SubKriteria', 'id_kriteria', 'id');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id_user', 'id');
    }
    public function kriteria1()
    {
        return $this->belongsTo('App\Models\Kriteria', 'id_kriteria_1', 'id');
    }
    public function kriteria2()
    {
        return $this->belongsTo('App\Models\Kriteria', 'id_kriteria_2', 'id');
    }
}
